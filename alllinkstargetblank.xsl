<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="@*|node()">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()"/>
        </xsl:copy>
    </xsl:template>

    <xsl:template match="a[@href]">
        <xsl:copy>
            <xsl:attribute name="target">_blank</xsl:attribute>
            <xsl:apply-templates select="@*[not(local-name()='target')]|node()"/>
        </xsl:copy>
    </xsl:template>
</xsl:stylesheet>
